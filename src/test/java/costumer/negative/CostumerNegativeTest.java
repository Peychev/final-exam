package costumer.negative;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CostumerNegativeTest {

    public WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\peych\\OneDrive\\Desktop\\Web drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://shop.pragmatic.bg/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void costumerTest () {
        WebElement myAccount=driver.findElement(By.xpath("//a[@title=\"My Account\"]"));
        myAccount.click();
        WebElement register=driver.findElement(By.xpath("//a[text()=\"Register\"]"));
        register.click();

        WebElement continueButon = driver.findElement(By.cssSelector("div.buttons input[type=submit]"));
        continueButon.click();

        WebElement errorFirstName=driver.findElement(By.cssSelector("div.col-sm-10 input#input-firstname+div.text-danger"));
        String elementErrorFirstName = errorFirstName.getText();
        Assert.assertEquals(elementErrorFirstName,"First Name must be between 1 and 32 characters!");
        System.out.println("First name not exist");

        WebElement errorLastName=driver.findElement(By.cssSelector("div.col-sm-10 input#input-lastname+div.text-danger"));
        String elementErrorLastName = errorLastName.getText();
        Assert.assertEquals(elementErrorLastName,"Last Name must be between 1 and 32 characters!");
        System.out.println("Last name not exist");

        WebElement errorEmail=driver.findElement(By.cssSelector("div.col-sm-10 input#input-email+div.text-danger"));
        String elementEmail = errorEmail.getText();
        Assert.assertEquals(elementEmail,"E-Mail Address does not appear to be valid!");
        System.out.println("Email not exist");

        WebElement errorTelephone=driver.findElement(By.cssSelector("div.col-sm-10 input#input-telephone+div.text-danger"));
        String elementTelephone = errorTelephone.getText();
        Assert.assertEquals(elementTelephone,"Telephone must be between 3 and 32 characters!");
        System.out.println("Telephone not exist");

        WebElement errorPassword=driver.findElement(By.cssSelector("div.col-sm-10 input#input-password+div.text-danger"));
        String elementPassword = errorPassword.getText();
        Assert.assertEquals(elementPassword,"Password must be between 4 and 20 characters!");
        System.out.println("Password not exist");

        WebElement errorWarning=driver.findElement(By.xpath("//div[@class=\"alert alert-danger alert-dismissible\"]"));
        String elementWarning = errorWarning.getText();
        Assert.assertEquals(elementWarning,"Warning: You must agree to the Privacy Policy!");
        System.out.println("WTF");
    }
}
