package costumer.positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CostumerPositiveTest {

    public WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\peych\\OneDrive\\Desktop\\Web drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://shop.pragmatic.bg/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void costumerTest () {
        WebElement myAccount=driver.findElement(By.xpath("//a[@title=\"My Account\"]"));
        myAccount.click();
        WebElement register=driver.findElement(By.xpath("//a[text()=\"Register\"]"));
        register.click();

        WebElement registrationForm=driver.findElement(By.className("form-horizontal"));
        Assert.assertEquals(true,registrationForm.isDisplayed());
        System.out.println("Registration form is displayed");

        WebElement firstName=driver.findElement(By.cssSelector("div.col-sm-10 input#input-firstname"));
        firstName.sendKeys("Marius");
        WebElement lastName=driver.findElement((By.cssSelector("div.col-sm-10 input#input-lastname")));
        lastName.sendKeys("Kurkinski");
        WebElement email=driver.findElement(By.cssSelector("div.col-sm-10 input#input-email"));
        email.sendKeys("palavnik@abv.bg");
        WebElement telephone = driver.findElement(By.cssSelector("div.col-sm-10 input#input-telephone"));
        telephone.sendKeys("0888886655");
        WebElement password = driver.findElement(By.cssSelector("div.col-sm-10 input#input-password"));
        password.sendKeys("1234");
        WebElement passwordConfirm = driver.findElement(By.cssSelector("div.col-sm-10 input#input-confirm"));
        passwordConfirm.sendKeys("1234");

        WebElement newsletter = driver.findElement(By.xpath("//div[@class='form-group']//label[@class='radio-inline']//input[@type='radio']"));
        if(!newsletter.isSelected()){
            newsletter.click();
        }
        Assert.assertTrue(newsletter.isSelected());

        WebElement checkBox = driver.findElement(By.cssSelector("div.buttons input[type=checkbox]"));
        checkBox.click();

        WebElement continueButon = driver.findElement(By.cssSelector("div.buttons input[type=submit]"));
        continueButon.click();

        WebElement successfulReg=driver.findElement(By.cssSelector("div#content"));
        String allGood=successfulReg.getText();
        Assert.assertEquals(allGood, "Congratulations! Your new account has been successfully created!");

    }

    @AfterClass

    public void tearDown () {
        driver.quit();
    }


}
