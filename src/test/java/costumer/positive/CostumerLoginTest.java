package costumer.positive;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

public class CostumerLoginTest {
    public WebDriver driver;

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\peych\\OneDrive\\Desktop\\Web drivers\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.get("http://shop.pragmatic.bg/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void costumerLogin () {
        WebElement myAccount=driver.findElement(By.xpath("//a[@title=\"My Account\"]"));
        myAccount.click();
        WebElement login=driver.findElement(By.xpath("//a[text()=\"Login\"]"));
        login.click();

        String title = driver.getTitle();
        Assert.assertEquals(title,"Account Login");



        WebElement email=driver.findElement(By.cssSelector("input#input-email"));
        email.sendKeys("palavnik@abv.bg");
        WebElement password=driver.findElement(By.cssSelector("input#input-password"));
        password.sendKeys("1234");
        WebElement loginBtn = driver.findElement(By.cssSelector("input.btn "));
        loginBtn.click();

        String title1=driver.getTitle();
        Assert.assertEquals(title1,"My Account");


    }
}
